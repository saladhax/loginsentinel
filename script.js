function check(){
    console.log("Something");
    document.getElementById("results").innerHTML = "Checking password...";
    var password = document.getElementById("password").value;
    var input = new FileReader();
    var file = "/rockyou.txt";
    var content = input.readAsText(file).split("\n");
    content.sort();
    var compareResult = compare(password, content);
    if(compareResult == 0){
        document.getElementById("results").innerHTML = "Your password was found! Change it now!";
    }
    else{
        document.getElementById("results").innerHTML = "Your password was not found! You don't need to change it!"
    }
}

function compare(password, content){
    var midpoint = content.length / 2;
    if (password.localeCompare(content[midpoint]) == 0){
        return 1;
    }
    else if (password.localeCompare(content[midpoint]) != 0 && content.length < 2){
        return 0;
    }
    else if (password.localeCompare(content[midpoint]) < 0 && content.length >= 2){
        var newContent = content.slice(0, midpoint - 1);
        return compare(password, newContent);
    }
    else if (password.localeCompare(content[midpoint]) > 0 && content.length >= 2){
        var newContent = content.slice(midpoint + 1, content.length - 1);
        return compare(password, newContent);
    }
}